import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'LoadingStatusEnum.dart';
import 'MyBloc.dart';
import 'MyEvent.dart';
import 'MyState.dart';

class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_){
      context.read<MyBloc>().add(LoadCatEvent());
    });
  }

  _getProgressIndicator() {
    return CircularProgressIndicator();
  }

  _getCat(String imageUrl) {
    return Image.network(
      imageUrl,
      fit: BoxFit.cover,
      height: double.infinity,
      width: double.infinity,
      loadingBuilder:(BuildContext context, Widget child, ImageChunkEvent loadingProgress) {
        if (loadingProgress == null) return child;
        return CircularProgressIndicator();
      },
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Test'), centerTitle: true),
      body: BlocBuilder<MyBloc, MyState>(
        builder: (context, MyState state) {
          return Center(
            child: state.loadingStatus == LoadingStatusEnum.LOADING ? 
              _getProgressIndicator() :
              _getCat(state.cat.imageUrl)
          );
        },
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
        FloatingActionButton(
          child: const Icon(Icons.refresh),
          onPressed: () => context.read<MyBloc>().add(LoadCatEvent())
        )
      ])
    );
  }
}