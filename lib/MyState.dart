import 'package:test_project/Cat.dart';
import 'LoadingStatusEnum.dart';

class MyState {

  final LoadingStatusEnum loadingStatus;
  final Cat cat;

  const MyState({
    this.loadingStatus = LoadingStatusEnum.LOADING,
    this.cat,
  });

  MyState copyWith({
    LoadingStatusEnum loadingStatus,
    Cat cat,
  }) {
    return MyState(
      loadingStatus: loadingStatus ?? this.loadingStatus,
      cat: cat ?? this.cat,
    );
  }
  
}