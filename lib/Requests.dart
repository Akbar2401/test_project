import 'package:dio/dio.dart';

class Requests {

  static Future getCat() async {
    Response result = await Dio().get(
      "https://api.thecatapi.com/v1/images/search",
      queryParameters: {"limit": 1},
    );
    return result.data;
  }
}