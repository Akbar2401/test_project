import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'MyBloc.dart';
import 'HomePage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => MyBloc(),
      child: MaterialApp(
        home: BlocProvider(
          create: (_) => MyBloc(),
          child: HomePage(),
        ),
      )
    );
  }
}
