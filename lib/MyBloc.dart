import 'package:flutter_bloc/flutter_bloc.dart';
import 'Requests.dart';
import 'Cat.dart';
import 'LoadingStatusEnum.dart';
import 'MyEvent.dart';
import 'MyState.dart';

class MyBloc extends Bloc<MyEvent, MyState> {

  MyBloc() : super(const MyState());

  _loadCat() async {
    var json = await Requests.getCat();
    Cat cat = Cat.fromJson(json[0]);
    add(LoadCatEvent(loadingStatus: LoadingStatusEnum.COMPLETE, cat:cat));
  }

  @override
  Stream<MyState> mapEventToState(event) async* {
    if (event is LoadCatEvent) {
      if(event.loadingStatus == null) {
        yield state.copyWith(loadingStatus: LoadingStatusEnum.LOADING);
        _loadCat();
      } else {
        yield state.copyWith(loadingStatus: event.loadingStatus, cat: event.cat);
      }
    }
  }
}