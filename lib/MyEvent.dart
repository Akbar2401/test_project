import 'package:test_project/Cat.dart';
import 'LoadingStatusEnum.dart';

abstract class MyEvent {
  const MyEvent();
}

class LoadCatEvent extends MyEvent {
  const LoadCatEvent({this.loadingStatus, this.cat});
  final LoadingStatusEnum loadingStatus;
  final Cat cat;
}
